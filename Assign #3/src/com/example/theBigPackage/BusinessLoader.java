package com.example.theBigPackage;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.widget.VideoView;

public class BusinessLoader {
	
	//Main Control Variable
	public static int numberOfMovies = 5;
		
	//DBAS Object
	private DBAdapter database;
	
	//Pre-Loaded DB Rows
	private String[] movieName = {"The Big Querty", "Tootles", "Tree", "Chez", "Tiny Monsters!"};
	private String[] movieAuthor = {"schmeow tootles magoo", "marry mcdingle goes to town", "jorge elsuavez von chexaton", "johnson cuppajoe is a nice guy...", "bill judd never backs down..."};
	private int[] moviePath = {R.raw.thequerty, R.raw.thetootles, R.raw.thetree, R.raw.thechez, R.raw.jarvir};
	private int[] movieThumb = {R.drawable.thequerty_thumb, R.drawable.thetootles_thumb, R.drawable.thetree_thumb, R.drawable.thechez_thumb, R.drawable.jarvir_thumb};
		
	private ArrayList<String> theList;
		
	
	public BusinessLoader(Context newCtx, String[] vidList) {
		
		this.database = new DBAdapter(newCtx);
		
		for(int i=0; i<vidList.length; i++) {
			this.theList.add(vidList[i]);
			
		}
		
				
	} //end constructor
		
	
	
	// Functions ---------------------------------------------
	
	public long[] setUpMoviez(String packageName) {
		
		long rowsReturned[] = new long[this.numberOfMovies];
		
		//Open DB for entry
		this.database = this.database.open();
		
		//Data Insertion
		
		for(int i=0; i<this.numberOfMovies; i++) {
		 	rowsReturned[i] = this.database.insertAMovie(this.getMovieName(i), this.getMovieAuthor(i), "//" + packageName + "/" + this.theList.get(i), 
		 					  	String.valueOf(this.getMovieThumb(i)));
			
		} 
		
		//Close DB
		this.database.closeDB();
		
		return rowsReturned;
		
	} //end function
	
	
	
	public String findMovieName(long rowToFind) {
		
		//Open DB for entry
		this.database = this.database.open();
		
		Cursor rowReturned = this.database.getMovie(rowToFind);
		String name = (rowReturned.getString(rowReturned.getColumnIndex("name")));
				
		//Close DB
		this.database.closeDB();
		
		return name;
		
	} //end function
	
	public String findMovieAuthor(long rowToFind) {
		
		//Open DB for entry
		this.database = this.database.open();
		
		Cursor rowReturned = this.database.getMovie(rowToFind);
		String author = (rowReturned.getString(rowReturned.getColumnIndex("author")));
				
		//Close DB
		this.database.closeDB();
		
		return author;
		
	} //end function
	
	public String findMoviePath(long rowToFind) {
		
		//Open DB for entry
		this.database = this.database.open();
		
		Cursor rowReturned = this.database.getMovie(rowToFind);
		String path = (rowReturned.getString(rowReturned.getColumnIndex("path")));
				
		//Close DB
		this.database.closeDB();
		
		return path;
		
	} //end function
	
	public String findMovieThumb(long rowToFind) {
		
		//Open DB for entry
		this.database = this.database.open();
		
		Cursor rowReturned = this.database.getMovie(rowToFind);
		String thumb = (rowReturned.getString(rowReturned.getColumnIndex("thumb")));
				
		//Close DB
		this.database.closeDB();
		
		return thumb;
		
	} //end function
	
	public int findMovieRating(long rowToFind) {
		
		//Open DB for entry
		this.database = this.database.open();
		
		Cursor rowReturned = this.database.getMovie(rowToFind);
		int rating = rowReturned.getColumnIndex("rating");
				
		//Close DB
		this.database.closeDB();
		
		return rating;
		
	} //end function
	
	public boolean updateMovieDescription(long rowToFind, String newReview) {
		
		//Open DB for entry
		this.database = this.database.open();
		
		Cursor rowReturned = this.database.getMovie(rowToFind);
		boolean validUpdate = this.database.updateMovie( rowToFind, 
									(rowReturned.getString(rowReturned.getColumnIndex("name"))),
								    (rowReturned.getString(rowReturned.getColumnIndex("author"))), 
								    (rowReturned.getString(rowReturned.getColumnIndex("path"))), 
								    (rowReturned.getString(rowReturned.getColumnIndex("thumb"))),
								    (rowReturned.getString(rowReturned.getColumnIndex("thumb"))) );
				
		//Close DB
		this.database.closeDB();
		
		return validUpdate;
		
	} //end function
	
	
	
	//Getters -------------------------------------------------
	
	public DBAdapter getDatabase() {
		return this.database;
		
	} //end function
	
	public int getNumOfMovies() {
		return this.numberOfMovies;
		
	} //end function
	
	private String getMovieName(int index) {
		return this.movieName[index];
		
	} //end function
	
	private String getMovieAuthor(int index) {
		return this.movieAuthor[index];
		
	} //end function
	
	private int getMoviePath(int index) {
		return this.moviePath[index];
		
	} //end function
	
	private int getMovieThumb(int index) {
		return this.movieThumb[index];
		
	} //end function
	
	
	//Setters -------------------------------------------------
	
	public void setNumOfMovieSize(int index) {
		this.numberOfMovies = index;
		
	} //end function
	
	
	
	
	
	
	
	public long[] setUpMovies(String packageName) {
		
		long rowsReturned[] = new long[this.numberOfMovies];
		
		//Open DB for entry
		this.database = this.database.open();
		
		//Data Insertion
		
		for(int i=0; i<this.numberOfMovies; i++) {
		 	rowsReturned[i] = this.database.insertAMovie(this.getMovieName(i), this.getMovieAuthor(i), "android.resource://" + packageName + "/" + this.getMoviePath(i), 
		 					  	String.valueOf(this.getMovieThumb(i)));
			
		} 
		
		//Close DB
		this.database.closeDB();
		
		return rowsReturned;
		
	} //end function
	
	
	
}
