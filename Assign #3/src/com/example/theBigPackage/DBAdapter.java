package com.example.theBigPackage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBAdapter extends SQLiteOpenHelper {
	
	//Static Member Vars
    public static final String KEY_ROWID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_AUTHOR = "author";
    public static final String KEY_PATH = "path";
    public static final String KEY_THUMB = "thumb";
    public static final String KEY_RATING = "rating"; 
    private static final String TAG = "DBAdapter";
    
    private static final String DATABASE_NAME = "movies5.db";
    private static final String DATABASE_TABLE = "movies";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE =
        "create table " + DATABASE_TABLE + " (" + KEY_ROWID + " integer primary key autoincrement, "
        		+ KEY_NAME + " text not null, " + KEY_AUTHOR + " text not null, " + KEY_PATH + 
        			" text not null, " + KEY_THUMB + " text not null, " + KEY_RATING + " text not null)";
    
    
    //Private Member Vars
    private final String[] columnNameArray = {"id", "name", "author", "path", "thumb", "rating"};
   
    private SQLiteDatabase db;
    
    
    //Constructor ---------------------------------------------------------------------------
    public DBAdapter(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
        
    }
        
    
    // --------------------------------------------------------------------------------------
    
    @Override
    public void onCreate(SQLiteDatabase db) {
    	try {
    		db.execSQL(DATABASE_CREATE);	
    	} catch (SQLException e) {
    		e.printStackTrace();
    	}
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
        onCreate(db);
    }
    
    
    // ------------------------------------------------------------------
    

    //Getters -----------------------------------------------------------
    
    public String getColumnName(int index) {
    	return this.columnNameArray[index];
    	
    } //end function
     
    public Cursor getAllMovies() {
        return db.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_NAME,
        		KEY_AUTHOR, KEY_PATH, KEY_THUMB, KEY_RATING}, null, null, null, null, null);
        
    } //end function

    public Cursor getMovie(long rowId) throws SQLException {
        Cursor mCursor =
                db.query(true, DATABASE_TABLE, new String[] {KEY_ROWID,
                KEY_NAME, KEY_AUTHOR, KEY_PATH, KEY_THUMB, KEY_RATING}, KEY_ROWID + "=" + rowId, null,
                null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        
        return mCursor;
        
    } //end function
    
    
    
    
    // ------------------------------------------------------------------
    
    public DBAdapter open() throws SQLException {
        this.db = this.getWritableDatabase();
        return this;
        
    } //end function
  
    public void closeDB() {
        this.close();
         
    } //end function
    
    public long insertAMovie(String name, String author, String pathing, String thumb) {
    	String placeHolder = "0";
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, name);
        initialValues.put(KEY_AUTHOR, author);
        initialValues.put(KEY_PATH, pathing);
        initialValues.put(KEY_THUMB, thumb);
        initialValues.put(KEY_RATING, placeHolder);
        
        return this.db.insert(DATABASE_TABLE, null, initialValues);
        
    } //end function
   
    public boolean deleteMovie(long rowId) {
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
        
    } //end function

    public boolean updateMovie(long rowId, String name, String author, String pathing, String thumb, String rating) {
        ContentValues args = new ContentValues();
        args.put(KEY_NAME, name);
        args.put(KEY_AUTHOR, author);
        args.put(KEY_PATH, pathing);
        args.put(KEY_THUMB, thumb);
        args.put(KEY_RATING, rating);
        
        return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
        
    } //end function
    
    
   
}
