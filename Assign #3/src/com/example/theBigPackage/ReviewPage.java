package com.example.theBigPackage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ReviewPage extends Activity {
	
	
	private TextView mainDisp;
	private String page;
	private EditText contentEditorWidget;
	private String currDescription = null;
	
	
	//Overrides -------------------------------------------------------------
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.review_layout);
		
		//Grab Passed Activity Value
		Bundle extras = getIntent().getExtras();  
		this.setPage(extras.getString("which"));
		
		this.mainDisp = (TextView) findViewById(R.id.reviewMainDisp);
		this.contentEditorWidget = (EditText) findViewById(R.id.contentEditorMain);
		
		this.mainDisp.setText(this.getPage());
		
	}
	
	@Override
	public void finish() {
	  
	  Intent data = new Intent();
	  data.putExtra("returnReview", this.currDescription);
	  data.putExtra("returnPage", this.getPage());
	  setResult(RESULT_OK, data);
	  super.finish();
	  
	} 
	
	
	
	//Functions ----------------------------------------------
	
	public void closeScreen() {
		finish();
		
	} //end function
	
	public void saveDescription(View v) {
		this.currDescription = this.getContentEditorWidget();				
		this.closeScreen();
		
	} //end function
	
	
	
	public String getPage() {
		return this.page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getContentEditorWidget() {
		return this.contentEditorWidget.getText().toString();
	}

	public void setContentEditorWidget(EditText contentEditorWidget) {
		this.contentEditorWidget = contentEditorWidget;
	}

	
	
	

}
