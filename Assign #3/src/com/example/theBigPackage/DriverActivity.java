package com.example.theBigPackage;

import java.io.IOException;
import java.util.ArrayList;
import com.example.theBigPackage.R;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.RatingBar;
import android.widget.VideoView;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class DriverActivity extends Activity {
	
	//Static Review Holder
	public static String currReviewToAdd = "X";
	private static final int REQUEST_CODE = 1;

	//GUI Objects --
	private EditText mainDisp;
	private ImageButton qwertyVidBtn;
	private ImageButton tootlesBtn;
	private ImageButton treeBtn;
	private ImageButton chezBtn;
	private ImageButton monsterBtn;
	private BusinessLoader program;
	private VideoView vid;
	private RatingBar rater;
	
	//Info Arrays
	private ArrayList<String> moviesDisplayNames;
	private ArrayList<String> movieAuthors;
	private ArrayList<String> moviePaths;
	private ArrayList<String> movieThumbs;
	private String[] x;
	private int[] movieRatings;
	private long[] rowsInserted;
	
	//Vars
	private String currButton;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		
		//Gather Resources
		this.mainDisp = (EditText) findViewById(R.id.mainTitleDisplay);
		this.qwertyVidBtn = (ImageButton) findViewById(R.id.qwertyVid);
		this.tootlesBtn = (ImageButton) findViewById(R.id.toodlesBtn);
		this.treeBtn = (ImageButton) findViewById(R.id.treeBtn);
		this.chezBtn = (ImageButton) findViewById(R.id.chezBtn);
		this.monsterBtn = (ImageButton) findViewById(R.id.monsterBtn);
		this.vid = (VideoView) findViewById(R.id.mainVideoDisplayer);
		this.rater = (RatingBar) findViewById(R.id.mainRatingBarDisp);
		
		//Grab Assets
		AssetManager assetMgr = this.getAssets();
		try {
			x = assetMgr.list("../raw/");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//Initialize Database & Pre-Loaded Movies
		this.program = new BusinessLoader(this, x);
		rowsInserted = this.program.setUpMovies(getPackageName());
				
		//Initialize Arrays
		this.moviesDisplayNames = new ArrayList(this.program.getNumOfMovies());
		this.moviePaths = new ArrayList(this.program.getNumOfMovies());
		this.movieAuthors = new ArrayList(this.program.getNumOfMovies());
		this.movieThumbs = new ArrayList(this.program.getNumOfMovies());
		
		//Begin Setting Up Arrays
		this.setUpMovieNames(this.program, rowsInserted);
		this.setUpMoviePaths(this.program, rowsInserted);
		this.setUpMovieAuthors(this.program, rowsInserted);
		this.setUpMovieThumbs(this.program, rowsInserted);
		this.setUpMovieRatings(this.program, rowsInserted);
		
		
		//Set Image Thumbnails
		this.qwertyVidBtn.setImageResource(Integer.valueOf(this.movieThumbs.get(0)));
		this.tootlesBtn.setImageResource(Integer.valueOf(this.movieThumbs.get(1)));
		this.treeBtn.setImageResource(Integer.valueOf(this.movieThumbs.get(2)));
		this.chezBtn.setImageResource(Integer.valueOf(this.movieThumbs.get(3)));
		this.monsterBtn.setImageResource(Integer.valueOf(this.movieThumbs.get(4)));
						
	}  //end of activity creation
	
		
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  
		if(data.getExtras().getString("returnPage").compareTo("one")==0) {
			boolean tester = this.program.updateMovieDescription(this.rowsInserted[0], data.getExtras().getString("returnReview"));
			this.movieAuthors.set(0, this.program.findMovieAuthor(this.rowsInserted[0]));
				
		}
				
	} //end of return results from intent

	
	
	//Functions --------------------------------------------------------------------
	
	private void setUpMovieNames(BusinessLoader prog, long[] rows) {
		for(int i=0; i<rows.length; i++) {
			this.moviesDisplayNames.add(prog.findMovieName(rows[i]));
			
		}
		
	} //end function
	
	
	private void setUpMovieAuthors(BusinessLoader prog, long[] rows) {
		for(int i=0; i<rows.length; i++) {
			this.movieAuthors.add(prog.findMovieAuthor(rows[i]));
			
		}
		
	} //end function
	
	
	private void setUpMoviePaths(BusinessLoader prog, long[] rows) {
		for(int i=0; i<rows.length; i++) {
			this.moviePaths.add(prog.findMoviePath(rows[i]));
			
		}
		
	} //end function
	
	private void setUpMovieThumbs(BusinessLoader prog, long[] rows) {
		for(int i=0; i<rows.length; i++) {
			this.movieThumbs.add(prog.findMovieThumb(rows[i]));
			
		}
		
	} //end function
		
	private void setUpMovieRatings(BusinessLoader prog, long[] rows) {
		this.movieRatings = new int[rows.length];
		for(int i=0; i<rows.length; i++) {
			this.movieRatings[i] = prog.findMovieRating(rows[i]);
			
		}
		
	} //end function
	
	
	
	// ====================================================================================
	
	
	
	//Click Events ------------------------------------------------------------------------
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void showVideoButtonOne(View v) {
				
		this.mainDisp.setText(this.moviesDisplayNames.get(0) + "- Desc: " + this.movieAuthors.get(0));
		this.rater.setRating(this.movieRatings[0]);
		this.currButton = "one";
		
		String urlPathing = this.moviePaths.get(0);
		
		this.vid.setVideoURI(Uri.parse(urlPathing));
		
		MediaController mc = new MediaController(this);
		mc.setMediaPlayer(this.vid);
		this.vid.setMediaController(mc);
		
		this.vid.start();
		
	} //end function
	
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void showVideoButtonTwo(View v) {
			
		this.mainDisp.setText(this.moviesDisplayNames.get(1) + "- Desc: " + this.movieAuthors.get(1));
		this.rater.setRating(this.movieRatings[1]);
		this.currButton = "two";
		
		String urlPathing = this.moviePaths.get(1);
		
		this.vid.setVideoURI(Uri.parse(urlPathing));
		
		MediaController mc = new MediaController(this);
		mc.setMediaPlayer(this.vid);
		this.vid.setMediaController(mc);
		
		this.vid.start();
		
	} //end function
	
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void showVideoButtonThree(View v) {
		
		this.mainDisp.setText(this.moviesDisplayNames.get(2) + "- Desc: " + this.movieAuthors.get(2));
		this.rater.setRating(this.movieRatings[2]);
		this.currButton = "three";
		
		String urlPathing = this.moviePaths.get(2);
		
		this.vid.setVideoURI(Uri.parse(urlPathing));
		
		MediaController mc = new MediaController(this);
		mc.setMediaPlayer(this.vid);
		this.vid.setMediaController(mc);
		
		this.vid.start();
		
	} //end function

	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void showVideoButtonFour(View v) {
		
		this.mainDisp.setText(this.moviesDisplayNames.get(3) + "- Desc: " + this.movieAuthors.get(3));
		this.rater.setRating(this.movieRatings[3]);
		this.currButton = "four";
		
		String urlPathing = this.moviePaths.get(3);
		
		this.vid.setVideoURI(Uri.parse(urlPathing));
		
		MediaController mc = new MediaController(this);
		mc.setMediaPlayer(this.vid);
		this.vid.setMediaController(mc);
		
		this.vid.start();
		
	} //end function
	
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void showVideoButtonFive(View v) {
		
		this.mainDisp.setText(this.moviesDisplayNames.get(4) + "- Desc: " + this.movieAuthors.get(4));
		this.rater.setRating(this.movieRatings[4]);
		this.currButton = "five";
		
		String urlPathing = this.moviePaths.get(4);
		
		this.vid.setVideoURI(Uri.parse(urlPathing));
		
		MediaController mc = new MediaController(this);
		mc.setMediaPlayer(this.vid);
		this.vid.setMediaController(mc);
		
		this.vid.start();
		
	} //end function
	
	
	public void launchReviewView(View v) {
		
		Intent intent = new Intent(v.getContext(), ReviewPage.class);
    	intent.putExtra("which", this.currButton);
    	startActivityForResult(intent, REQUEST_CODE);
		
	} //end function
	
	
			
	

}
